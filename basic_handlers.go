package main

import "net/http"

func Root() http.Handler {
	return http.FileServer(http.Dir(StaticRoot))
}

func Finda() http.Handler {
	return http.FileServer(http.Dir(FindaRoot))
}
