#!/bin/sh
git submodule update

pushd finda > /dev/null
if [ ! dist/index.html -nt index.html ]; then
    npm install
    npm run build
    rm dist/config.json dist/data.geojson dist/styles/properties.css
    rm -rf ../finda_dist
    mkdir ../finda_dist
    mv dist/* ../finda_dist
fi
popd > /dev/null

godep go build
godep go install
