package main

import (
	"fmt"
	"os"
	"path/filepath"
)

const (
	FindaRoot  = `finda_dist`
	StaticRoot = `static`
	SHALength  = 20
)

var FindaFiles = FilesUnderneath(FindaRoot)

var ServerPort = Port()

func Port() (port string) {
	if port = os.Getenv("PORT"); port == "" {
		port = "8080"
	}
	return
}

func FilesUnderneath(root string) (files map[string]bool) {
	files = make(map[string]bool)
	filepath.Walk(root,
		func(path string, info os.FileInfo, err error) error {
			if err != nil || info.IsDir() {
				return nil
			}
			files[path[len(root)+1:]] = true
			return nil
		})
	return
}

func init() {
	fmt.Println("found Finda files", FindaFiles)
}
