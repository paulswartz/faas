package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	fmt.Println("Running FaaS on", ServerPort)
	log.Fatal(http.ListenAndServe(":"+ServerPort, FindaMux()))
}
