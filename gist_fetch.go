package main

import (
	"code.google.com/p/goauth2/oauth"
	"fmt"
	"github.com/lox/httpcache"
	"github.com/paulswartz/go-github/github"
	"net/http"
	"os"
	"strings"
	"time"
)

type GistJob struct {
	GistID   string
	Gist     *github.Gist
	Response *github.Response
	Error    error
}

type GistRequest struct {
	GistID string
	Result chan *GistJob
}

var gistRequestQueue chan *GistRequest

var globalClient = client()

func client() *github.Client {
	var token = os.Getenv("GITHUB_TOKEN")
	var transport = http.DefaultClient

	if token != "" {
		t := &oauth.Transport{
			Token: &oauth.Token{AccessToken: token},
		}
		transport = t.Client()
	}
	return github.NewClient(transport)
}

func gistWorkerPool(n int) (jobs chan *GistJob, results chan *GistJob) {
	jobs = make(chan *GistJob)
	results = make(chan *GistJob)

	for i := 0; i < n; i++ {
		go gistWorker(jobs, results)
	}

	return
}

func gistWorker(jobs chan *GistJob, results chan *GistJob) {
	for job := range jobs {
		fmt.Println("fetching", job.GistID)
		gist, response, err := globalClient.Gists.Get(job.GistID)
		job.Gist = gist
		job.Response = response
		job.Error = err
		results <- job
	}
}

func isValid(job *GistJob) bool {
	// no existing values, so not valid
	if job == nil {
		return false
	}
	response := job.Response

	// if the current data isn't expired, then it's good
	cc, _ := httpcache.ParseCacheControlHeaders(response.Header)
	duration, err := cc.Duration(`max-age`)
	if err != nil {
		return false
	}
	date, err := time.Parse(time.RFC1123, response.Header.Get(`Date`))
	if err != nil {
		return false
	}
	expiresAt := date.Add(duration)
	if time.Now().UTC().Before(expiresAt) {
		return true
	}

	// make a HEAD request to see if the data's been updated
	request, err := http.NewRequest(`HEAD`, response.Request.URL.String(), nil)
	if err != nil {
		return false
	}
	request.Header.Add(`If-Modified-Since`, response.Header.Get(`Last-Modified`))
	headResponse, err := http.DefaultClient.Do(request)

	if err != nil {
		return false
	}
	return headResponse.StatusCode == 304
}

func gistCache(upstreamJobs chan *GistJob, upstreamResults chan *GistJob) (jobs chan *GistJob, results chan *GistJob) {
	jobs = make(chan *GistJob)
	results = make(chan *GistJob)

	go func() {
		cache := make(map[string]*GistJob)

		for {
			select {
			case job := <-jobs:
				cachedJob := cache[job.GistID]
				if isValid(cachedJob) {
					results <- cachedJob
				} else {
					upstreamJobs <- job
				}
			case result := <-upstreamResults:
				cache[result.GistID] = result
				results <- result
			}
		}
	}()
	return
}

func gistRequestMux(jobs chan *GistJob, results chan *GistJob) (requests chan *GistRequest) {
	requests = make(chan *GistRequest)

	go func() {
		queues := make(map[string][]chan *GistJob)
		for {
			select {
			case request := <-requests:
				queues[request.GistID] = append(queues[request.GistID], request.Result)
				if len(queues[request.GistID]) == 1 {
					go func() {
						jobs <- &GistJob{GistID: request.GistID}
					}()
				}
			case job := <-results:
				for _, result := range queues[job.GistID] {
					result <- job
					close(result)
				}
				delete(queues, job.GistID)
			}
		}
	}()

	return
}

func newGistRequest(gistID string) *GistRequest {
	return &GistRequest{GistID: gistID, Result: make(chan *GistJob)}
}

func FetchGist(gistID string) (gist *github.Gist, response *github.Response, err error) {
	request := newGistRequest(gistID)
	gistRequestQueue <- request
	job := <-request.Result
	return job.Gist, job.Response, job.Error
}

func FileFromGist(gist *github.Gist, path string) (file github.GistFile, err error) {
	file, ok := gist.Files[github.GistFilename(path)]
	if !ok {
		// try looking up just the filename
		split := strings.Split(path, "/")
		filename := split[len(split)-1]
		file, ok = gist.Files[github.GistFilename(filename)]
	}
	if !ok {
		err = fmt.Errorf("gist file %s not found in gist %s", path, *gist.ID)
	}
	return
}

func init() {
	jobs, results := gistWorkerPool(5)
	jobs, results = gistCache(jobs, results)

	gistRequestQueue = gistRequestMux(jobs, results)
}
