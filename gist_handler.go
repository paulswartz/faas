package main

import (
	"fmt"
	"github.com/paulswartz/go-github/github"
	"net/http"
	"strings"
)

func NotFound(rw http.ResponseWriter) {
	rw.WriteHeader(http.StatusNotFound)
	rw.Write([]byte("404 not found"))
}

func GistHandler(gistID string) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		var file github.GistFile
		gist, response, err := FetchGist(gistID)
		if err == nil {
			file, err = FileFromGist(gist, r.URL.Path)
		}

		if err != nil {
			fmt.Println(err)
			NotFound(rw)
			return
		}

		header := rw.Header()
		header.Set("Content-Type", *file.Type)
		header.Set("Cache-Control", response.Header.Get("Cache-Control"))

		if *file.Truncated == true {
			http.Redirect(rw, r, *file.RawURL, http.StatusFound)
			return
		}
		http.ServeContent(rw, r, *file.Filename, *gist.UpdatedAt,
			strings.NewReader(*file.Content))
	})
}
