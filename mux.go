package main

import (
	"net/http"
	"strings"
)

func FindaMux() http.Handler {
	root := Root()
	finda := Finda()
	gistHandlers := make(map[string]http.Handler)

	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		split := strings.SplitN(r.URL.Path, "/", 3)
		if len(split) != 3 {
			root.ServeHTTP(rw, r)
			return
		}

		gistID, path := split[1], split[2]

		if len(gistID) != SHALength {
			root.ServeHTTP(rw, r)
			return
		}

		r.URL.Path = path

		if ok := FindaFiles[path]; path == "" || ok {
			finda.ServeHTTP(rw, r)
			return
		}

		gistHandler, ok := gistHandlers[gistID]
		if !ok {
			gistHandler = GistHandler(gistID)
			gistHandlers[gistID] = gistHandler
		}
		gistHandler.ServeHTTP(rw, r)
	})
}
