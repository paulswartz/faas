// global $
$(document).ready(function() {
  "use strict";
  var shaRegex = /(\/|^)([0-9a-fA-F]{20})$/;
  $("#enter").submit(function(ev) {
    ev.preventDefault();
    var shaOrURL = $("#url_or_sha").val();
    var result = shaRegex.exec(shaOrURL);
    if (result === null) {
      alert("Sorry, we couldn't find a SHA or a Gist URL.");
      return;
    }
    window.location.href = "/" + result[2] + "/";
  });
});
